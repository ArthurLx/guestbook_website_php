<?php
	session_start();
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<title>GuestBook Search</title>
	<link rel="stylesheet" type="text/css" href="../style.css" >
	<link rel="stylesheet" type="text/css" href="../account/login_style.css">
</head>
<body>
	
	<header>
		<div class="flex-container">
			<?php
				if (!isset($_SESSION['login'])) {
					echo '<a href="../Index.php"><div class="flex-elem">Главная</div></a>';
				}
				else if ($_SESSION['login'] == 'Admin') {
					echo '<a href="../admin/IndexAdmin.php"><div class="flex-elem">Главная</div></a>';
				}
				else {
					echo '<a href="../user/IndexUser.php"><div class="flex-elem">Главная</div></a>';
				}
			?>
			<a href="../search/SearchForm.php"><div class="flex-elem">Поиск</div></a>
			<a href="../account/Register.php"><div class="flex-elem">Регистрация</div></a>
			<?php
				if (isset($_SESSION['login'])) {
					echo '<a href="../user/Logout.php"><div class="flex-elem">Выйти</div></a>';
				}
				else {
					echo '<a href="../account/Login.php"><div class="flex-elem">Войти</div></a>';
				}
			?>
			
		</div>
	</header>
	<br><br>
	<main>
		
		<form action="Search.php" class="form" method="POST">
			<h2 align="center">Поиск сообщений в Базе Данных</h2> <br>
			<textarea id="text_area" name="message1" placeholder="Введите часть сообщения" 
					oninput="textarea_auto_expand(this)" required></textarea>
			<button class="form-button submit-button" type="submit" name="update">Поиск</button>
		</form>
		
		<?php 
			echo $_SESSION['message'];
			$_SESSION['message'] = NULL;
		?>

	</main>
	

</body>
<script>
	function textarea_auto_expand(element) {
		element.style.height = "20px";
		element.style.height = (element.scrollHeight)+"px";
	}
</script>
</html>