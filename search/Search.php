<?php
require "../connectToDB.php";

session_start();

$message1 = trim($_POST['message1']);
if(empty($_POST['message1'])) return;

$query = "SELECT * FROM `messages` WHERE `message` LIKE '%$message1%'";
$result = mysqli_query($connection, $query);


if(!$result) echo mysqli_error($connection);
else {
    $num = mysqli_num_rows($result);

    $_SESSION['message'] = '';
	if($num > 0){
        $_SESSION['message'] .= '<table><tr><th>id</th><th>time</th><th>date</th><th>name</th><th>email</th><th>message</th></tr>';
        while($row=mysqli_fetch_array($result, MYSQLI_ASSOC)){
            
            if ($_SESSION['login'] == 'Admin') {
                $_SESSION['message'] .= '<tr><td>'.$row['id'].'</td><td>'.$row['time'].'</td><td>'.$row['date'].'</td><td>'.$row['name'].'</td><td>'.$row['email'].'</td><td>'.$row['message'].'</td><td>'.
                    '<a href="../CRUD/UpdateForm.php?id='.$row['id'].'">Update</a>'.
                    '</td><td>'.
                    '<a href="../CRUD/Delete.php?id='.$row['id'].'">Delete</a>'.
                    '</td></tr>';
            }
            else {
                if ($row['name'] == $_SESSION['login']) {
                    $_SESSION['message'] .= '<tr><td>'.$row['id'].'</td><td>'.$row['time'].'</td><td>'.$row['date'].'</td><td>'.$row['name'].'</td><td>'.$row['email'].'</td><td>'.$row['message'].'</td><td>'.
                    '<a href="../CRUD/UpdateForm.php?id='.$row['id'].'">Update</a>'.
                    '</td><td>'.
                    '<a href="../CRUD/Delete.php?id='.$row['id'].'">Delete</a>'.
                    '</td></tr>';
                }
                else {
                    $_SESSION['message'] .= '<tr><td>'.$row['id'].'</td><td>'.$row['time'].'</td><td>'.$row['date'].'</td><td>'.$row['name'].'</td><td>'.$row['email'].'</td><td>'.$row['message'].'</td><td></td><td></td>';
                }
            }
        };
        $_SESSION['message'] .= '</table>';
    } else {
        $_SESSION['message'] .= '<h4 align="center">Ничего не найдено</h4>';
    }
    header('Location: SearchForm.php');
}
?>