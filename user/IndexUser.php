<?php 
	session_start();
	if(!isset($_SESSION['login'])){
        header('Location: /Index.php');
		exit();
    }
	include "get_user_data.php";
?>

<!DOCTYPE html>
<html>
<head> 
	<title>User GuestBook</title>
	<link href="../style.css" rel="stylesheet">
</head>
<body>
	
	<header>
		<div class="flex-container">
			<a href="../user/IndexUser.php"><div class="flex-elem">Главная</div></a>
			<a href="../search/SearchForm.php"><div class="flex-elem">Поиск</div></a>
			<a href="../account/Register.php"><div class="flex-elem">Регистрация</div></a>
			<a href="../user/Logout.php"><div class="flex-elem">Выйти</div></a>
		</div>
	</header>
	
	<main>
		<br>

		<div class="grid-container">
			<div class="grid-elem"></div>

			<h1 align="center" class="grid-elem grid-title">Гостевая книга</h1>

			<div class="grid-elem">
				<div class="user-greeting">
					<?php 
						echo 'Welcome ';
						echo $_SESSION['login'];
					?>
					<br>
					<div class="user-info">
						Member since: <?php echo $_SESSION['date']; ?>
					</div>
				</div>
			</div>
		</div>
		<br>
		<div align="center">
			<?php include_once "PrintDB_user.php"; ?>
		</div>
		
		<div class="floating_block">
			<img src = "../images/phone.png" alt= "css_logo" width="70" height="70"/>
			Reception: 0-800-111-111 Manager: 050-111-1111 <br> Address: Center 7 Street, Kyiv, UA01001
		</div>
		
		<br><br>

		<div align="center">
			<h3 >Оставить сообщение: </h3>

			<form action="../CRUD/Insert.php" class="form" method="POST" id="blur">
				
				<div class="input_field">
					<input type="text" id="login" name="login"
						placeholder=<?php echo $_SESSION['login'];?> 
						disabled 
					/>
				</div>

				<div class="input_field">
					<input type="email" id="email" name="email"
						placeholder=<?php echo $_SESSION['email']; ?> 
						disabled 
					/>
				</div>
				
				<div class="input_field">
					<!-- <input type="text" id="notblur" name="message" placeholder="Сообщение" required /> -->
					<textarea id="text_area" name="message" placeholder="Сообщение" oninput="textarea_auto_expand(this)"  required></textarea>
				</div>
				<br>
				<button class = "form-button" type="submit" name="insert">Подтвердить</button>
			</form>
			
			<br>
			<a href="../search/SearchForm.php">Найти сообщение</a>
		</div>
	</main>

    <br><br><br><br><br><br>

</body>
<script>
	function textarea_auto_expand(element) {
		element.style.height = "20px";
		element.style.height = (element.scrollHeight)+"px";
	}
</script>
</html>
