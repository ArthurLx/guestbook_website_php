<?php 
    session_start(); 
    
	if (isset($_SESSION['login'])) {
		if ($_SESSION['login'] == 'Admin') {
			header('Location: ../admin/IndexAdmin.php');
		}
		else {
			header('Location: ../user/IndexUser.php');
		}
	}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <link rel="stylesheet" type="text/css" href="../style.css">
    <link rel="stylesheet" type="text/css" href="login_style.css">
    <style>
        .form input:valid:not(:placeholder-shown) {
            border: 1px dashed #FFA172;
        }
    </style>
    <title>Login</title>
</head>
<body>
    <header>
        <div class="flex-container">
            <a href="../Index.php"><div class="flex-elem">Главная</div></a>
			<a href="../search/SearchForm.php"><div class="flex-elem">Поиск</div></a>
			<a href="../account/Register.php"><div class="flex-elem">Регистрация</div></a>
			<a href="../account/Login.php"><div class="flex-elem">Вход</div></a>
        </div>
    </header>
    <br><br>
    <main>
        <form action="LoginBack.php" class="form" method="POST">
        
            <h2 align="center">Вход в аккаунт</h2><br>
            
            <div class="input_field">
                <input  id="login" name="login" type="text" placeholder="Name" required />
            </div>
            
            <div class="input_field">
                <input id="pwd" name="password" type="password" placeholder="Password" required />
            </div>
            
            <?php 
                echo $_SESSION['message']; 
                $_SESSION["message"] = null;
            ?>

            <button name="signin" class ="form-button submit-button" type="submit">Подтвердить</button>

            <div>
                <button class="interact-button" type="button">Сбросить пароль</button>
                <a href="Register.php"><button class="interact-button" type="button">Зарегестрироваться</button></a>
            </div>
            
        </form>
    </main>
</body>
</html>