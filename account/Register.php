<?php 
	session_start();

	if (isset($_SESSION['login'])) {
		if ($_SESSION['login'] == 'Admin') {
			header('Location: ../admin/IndexAdmin.php');
		}
		else {
			header('Location: ../user/IndexUser.php');
		}
	}
?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<link rel="stylesheet" type="text/css" href="../style.css">
		<link rel="stylesheet" type="text/css" href="login_style.css">

		<title>Registration</title>
	</head>
  
<body>
	<header>
		<div class="flex-container">
			<a href="../Index.php"><div class="flex-elem">Главная</div></a>
			<a href="../search/SearchForm.php"><div class="flex-elem">Поиск</div></a>
			<a href="../account/Register.php"><div class="flex-elem">Регистрация</div></a>
			<a href="../account/Login.php"><div class="flex-elem">Вход</div></a>
		</div>
	</header>
	<br><br>
	<main>
		<form action="RegistrationBack.php" class="form" method="POST">
			
			<h2 align="center">Регистрация</h2><br>
			
			<div class="input_field">
				<input  id="login" name="login" type="text" placeholder="Name*" pattern="[a-zA-Z]{2,}$" required />
				<span class="input_error">Разрешены символы a-z, A-Z,</span>
			</div>
			
			<div class="input_field">
				<input id="email" name="email" type="email" placeholder="E-Mail*" pattern="[a-zA-Z0-9._%]+@[a-z0-9_]+\.[a-z]{2,}$" required />
				<span class="input_error">Формат E-Mail адреса words@mail.damain разрешенные символы a-z, A-Z, 0-9, _, . </span>
			</div>
			
			<div class="input_field">
				<input type="number" placeholder="Age" />
			</div>

			<div class="input_field">
				<input type="text" placeholder="Address" />
			</div>

			<div class="input_field">
				<input type="tel" placeholder="Phone number" pattern="0\d{8}" />
				<span class="input_error">Формат номера 079459034</span>
			</div>
			
			<div class="input_field">
				<input id="pwd" name="password1" type="password" placeholder="Password*" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}$" required />
				<span class="input_error">Валидный пароль должен иметь одну большую букву одну маленькую и иметь 8 символов или больше</span>
			</div>
			
			<div class="input_field">
				<input id="pwd" name="password2" type="password" placeholder="Password Confirm*" required />
			</div>
			
			<?php 
                echo $_SESSION['message']; 
                $_SESSION["message"] = null;
            ?>

			<button name="signin" class = "form-button submit-button" type="submit">Подтвердить</button>

		</form>
	</main>
	<br><br>
</body>
</html>