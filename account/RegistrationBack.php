<?php
    session_start();
    require $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR."connectToDB.php";

    function reject($field) {
        $_SESSION['message'] = "<span>Wrong $field type! Please, enter $field again...</span>";
        header('Location: Register.php');
        exit();
    }

    function noLen($field) {
        $_SESSION['message'] = "<span>Invalid $field length! Please, enter $field again...</span>";
        header('Location: Register.php');
        exit();
    }

    function alExist($field) {
        $_SESSION['message'] = "<span>This $field already exist! Please, enter $field again...</span>";
        header('Location: Register.php');
        exit();
    }

    function notEqual() {
        $_SESSION['message'] = '<p id="err">Not Equal Passwords</p>
        <p id="err">Please, sign-up again...</p>';
        header('Location: Register.php');
        exit();
    }
    
    if(isset($_POST['login']) && $_POST['login']!="") {
        $login=trim($_POST['login']);
        if(!ctype_alnum($login)) {
            reject('Login');
        } 
        else{
            if(strlen($login)<3 or strlen($login)>30){
                noLen('login');
            } 
            else {
                //check login
                $query = "SELECT * FROM `users` WHERE `login`='$login'";
                $result=mysqli_query($connection, $query);
                $num=mysqli_num_rows($result);
                if($num>0) {
                    alExist('login');
                } 
                else {
                    if(isset($_POST['password1'])) {
                        $passw1=trim($_POST['password1']);
                        if(!ctype_alnum($passw1)) {
                            reject('Password');
                        } 
                        else {
                            if(strlen($passw1)<3 or strlen($passw1)>30){
                                noLen('password');
                            } 
                            else {
                                if(isset($_POST['password2'])){
                                    $passw2=trim($_POST['password2']);
                                    if(!ctype_alnum($passw2)) {
                                        reject('Password Confirmation');
                                    } 
                                    else {
                                        if($passw1!=$passw2){
                                            notEqual();
                                        } 
                                        else {
                                            if(isset($_POST['email'])){
                                                $email=strip_tags(trim($_POST['email']));
                                                if(filter_var($email, FILTER_VALIDATE_EMAIL) == false) {
                                                    reject('Email');
                                                } 
                                                else {
                                                    $query = "SELECT * FROM `users` WHERE `email`='$email'";
                                                    $result=mysqli_query($connection, $query);
                                                    $num=mysqli_num_rows($result);
                            
                                                    if($num == 0){
                                                        $pass = password_hash($passw1, PASSWORD_DEFAULT);
                                                        $email_hash = md5($email);
                                                        date_default_timezone_set('Europe/Kiev');
                                                        $time = date("H:i:s");
                                                        $date = date("d/m/y");
                                                        
                                                        $query = "INSERT INTO `users` (`id`, `login`, `password`, `email`, `email_hash`, `email_confirmed`,`time`,`date`,`role`) 
                                                                VALUES (NULL, '$login', '$pass', '$email', '$email_hash', 1, '$time','$date',0)";

                                                        mysqli_query($connection, $query);
                                                        $_SESSION['message'] = "<span>Registration completed successfully!</span>";

                                                        header('Location: Register.php');
                                                    }
                                                    else{
                                                        alExist('email');
                                                    }
                                                }
                                            } 
                                            else {
                                                reject('email');
                                            }
                                        }
                                    }
                                } 
                                else {
                                    reject('Confirm password');
                                }
                            }
                        }
                    } 
                    else {
                        reject('password');
                    }
                }
            }
        }
    } 
    else {
        header('Location: Register.php');
    };      
?>