<?php
	require "../connectToDB.php"; 

	$id = $_GET['id'];

	//---------------------------LAST RECORD IN TABLE CHECK AND SAVE
	$query = "SELECT * FROM messages ORDER BY id DESC LIMIT 1";
	$result = mysqli_query($connection, $query);
	if(!$result) echo mysqli_error($connection); 

	$row = mysqli_fetch_array($result, MYSQLI_ASSOC);

	if($row['id'] == $id) {
		$last_in_table_selected = true;
	}
	else {
		$last_in_table_selected = false;
	}

	//---------------------------DELETE
	$query = "DELETE FROM `messages` WHERE `messages`.`id`=$id";
	$result = mysqli_query($connection, $query);
	if(!$result) echo mysqli_error($connection);

	//---------------------------ONE REMAINING RECORD DELETION CHECK, TO DROP AUTO_INCREMENT
	$query = 'SELECT * FROM `messages`';
	$result = mysqli_query($connection, $query);
	if(!$result) echo mysqli_error($connection);
	$num = mysqli_num_rows($result);
	
	if($num == 0) {
		$query = "ALTER TABLE `messages` AUTO_INCREMENT = 1";
		$result = mysqli_query($connection, $query);
		if(!$result) echo mysqli_error($connection); 
		exit();
	}

	//---------------------------LAST RECORD IN TABLE SELECTED TRUE
	if($last_in_table_selected) {

		//---------------------------CHECK PREVIOUS RECORD, EITHER ITS ID LESS BY ONE OR LESS BY MORE
		$query = "SELECT * FROM messages ORDER BY id DESC LIMIT 1";
		$result = mysqli_query($connection, $query);
		if(!$result) echo mysqli_error($connection); 

		$row = mysqli_fetch_array($result, MYSQLI_ASSOC);

		if($row['id'] == $id - 1) {	//Its id is less by one
			$query = "ALTER TABLE `messages` AUTO_INCREMENT = $id";
			$result = mysqli_query($connection, $query);

			if(!$result) echo mysqli_error($connection); 
		}
		else { // If $row['id'] < $id - 1 //Its id is less by more
			$ai_new_val = $row['id'] + 1;
			$query = "ALTER TABLE `messages` AUTO_INCREMENT = $ai_new_val";
			$result = mysqli_query($connection, $query);

			if(!$result) echo mysqli_error($connection); 
		}
	}
?>