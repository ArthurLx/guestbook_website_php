<?php
    require "../connectToDB.php";
    session_start();
    
    if(empty($_POST['message'])) {
        header('Location: ../user/IndexUser.php');
        exit();
    }

    date_default_timezone_set('Europe/Kiev');

    if(isset($_POST["insert"])){
        $login = $_SESSION['login'];
        $email = $_SESSION['email'];
        $message = htmlspecialchars(trim($_POST['message']));
        $ftime = date("H:i:s");
        $fdate = date("d/m/y");

        $query = "INSERT INTO `messages`(`id`,`time`,`date`,`name`,`email`,`message`) VALUES (NULL, '$ftime','$fdate','$login','$email','$message')";
        $result = mysqli_query($connection, $query);
        if(!$result) echo mysqli_error($connection);
        else {
            if ($_SESSION['login'] == 'Admin') {
                header('Location: ../admin/IndexAdmin.php');
            }
            else {                                
                header('Location: ../user/IndexUser.php');
            }
        }
    }

?>