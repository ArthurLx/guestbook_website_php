<?php
	require "../connectToDB.php";
	session_start();

	if(empty($_POST['message'])) {
		header('Location: ../user/IndexUser.php');
        exit();
	}

	date_default_timezone_set('Europe/Kiev');

	if(isset($_POST["update"])){
		$id = $_SESSION['record_id'];
		$message = htmlspecialchars(trim($_POST['message']));
		$ftime = date("H:i:s");
		$fdate = date("d/m/y");

		$query = "UPDATE `messages` SET `time`='$ftime', `date`='$fdate', `message`='$message' WHERE `messages`.`id`=$id";
		$result = mysqli_query($connection, $query);
		if(!$result) echo mysqli_error($connection);
        else {
            if ($_SESSION['login'] == 'Admin') {
                header('Location: ../admin/IndexAdmin.php');
            }
            else {                                
                header('Location: ../user/IndexUser.php');
            }
        }
	}
?>