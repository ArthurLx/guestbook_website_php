<?php
    require "../connectToDB.php";

	session_start();

    if(empty($_GET['id'])) return;
    $id = $_GET['id'];

    $query = "SELECT * FROM `messages` WHERE `messages`.`id`=$id";
    $result = mysqli_query($connection, $query);
    $row = mysqli_fetch_array($result, MYSQLI_ASSOC);

    $_SESSION['record_id'] = $row['id'];
	
	$user_name = $row['name'];	
    $message = $row['message'];
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <title>Update Review</title>
	<link href="../style.css" rel="stylesheet">
	<link href="../account/login_style.css" rel="stylesheet">
</head>
<body>
	<header>
		<div class="flex-container">
			<?php
				if ($_SESSION['login'] == 'Admin') {
					echo '<a href="../admin/IndexAdmin.php"><div class="flex-elem">Главная</div></a>';
				}
				else {
					echo '<a href="../user/IndexUser.php"><div class="flex-elem">Главная</div></a>';
				}
			?>
			<a href="../search/SearchForm.php"><div class="flex-elem">Поиск</div></a>
			<a href="../account/Register.php"><div class="flex-elem">Регистрация</div></a>
			<a href="../user/Logout.php"><div class="flex-elem">Выйти</div></a>
		</div>
	</header>
    <br><br>
	<main>		
		
		<form action="Update.php" class="form" method="POST">
			
			<h2 align="center">Изменить отзыв клиента <?=$user_name?></h2><br>

			<div class="input_field">
				<input type="text" name="id" value="<?=$_SESSION['record_id']?>" disabled />
			</div>
			
			<div class="input_field">
				<input type="text" id="fio" name="fio" value="<?=$_SESSION['login']?>" disabled />
			</div>
			
			<div class="input_field">
				<input type="email" id="email" name="email" value="<?=$_SESSION['email']?>" disabled/>
			</div>
			
			<div class="input_field">
				<textarea id="text_area" name="message" placeholder="Сообщение" 
					oninput="textarea_auto_expand(this)" required><?=$message?></textarea>
				<script>
					var element = document.getElementById('text_area');
					element.style.height = (element.scrollHeight)+"px";
				</script>
			</div>
			
			<button class="form-button submit-button" type="submit" name="update">Подтвердить</button>
			
		</form>

	</main>

</body>
<script>
	function textarea_auto_expand(element) {
		element.style.height = "20px";
		element.style.height = (element.scrollHeight)+"px";
	}
</script>
</html>