## Project Information:

This is the website of the hotel guestbook, where clients can leave reviews or short messages.  
Login and registration features are available.

All backend logic is php, frontend is html, css, js without frameworks.  
Mysql database as the data storage.

## How to set up:
To set up you need web server, php and mysql modules.  
Can be installed and configured separately or you can use web server solution stack packages like XAMPP or Open Server.

## How to start up:
When the first startup go to admin/IndexAdmin.php file and comment first prompted section then open /admin/IndexAdmin.php in browser  
that will initialize database with all tables and will create an admin account.  
After that the website can be hosted.