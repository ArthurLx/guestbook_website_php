<?php
	require "../connectToDB.php";
	session_start();

	$query = 'SELECT * FROM `messages`';

	$result = mysqli_query($connection, $query);

	if($result){
		$num = mysqli_num_rows($result);
		
		if($num > 0){
			echo '<br>';
			
			echo '<div class="tbl-header">';
				echo '<table cellpadding="0" cellspacing="0" border="0">
						<thead>
							<tr>
								<th>id</th>
								<th>time</th>
								<th>date</th>
								<th>name</th>
								<th>email</th>
								<th>message</th>
								<th></th>
								<th></th>
							</tr>
						</thead>
					</table>';
			echo '</div>';

			echo '<div class="tbl-content">';
				echo '<table cellpadding="0" cellspacing="0" border="0">';
				echo '<tbody>';
				while($row=mysqli_fetch_array($result, MYSQLI_ASSOC)){
					echo '<tr><td>'.$row['id'].'</td><td>'.$row['time'].'</td><td>'.$row['date'].'</td><td>'.$row['name'].'</td><td>'.$row['email'].'</td><td>'.$row['message'].'</td><td>'.
					'<a href="../CRUD/UpdateForm.php?id='.$row['id'].'">Update</a>'.
					'</td><td>'.
					'<a href="../CRUD/Delete.php?id='.$row['id'].'">Delete</a>'.
					'</td></tr>';
				};
				echo '</tbody>';
				echo '</table>';
			echo '</div>';
		} else echo "Нет записей";
	} else echo "Нет таблицы";
?>