<?php
require "../connectToDB.php"; 

date_default_timezone_set('Europe/Kiev');
echo "<p align='center'>Текущая дата: ".date("d/m/y")."</p>";
echo "<p align='center'>Текущее время: ".date("h:i:s")."</p>";

$query = "SELECT `count` FROM `views`";
$result = mysqli_query($connection, $query);

if ($result===FALSE) {
    echo "Ошибка";
    return;
};

$num = mysqli_num_rows($result);
if ($num==0) {
    echo "<p align='center'>Данная веб-страница была посещена: 1 раз</p>";
    $query = "INSERT INTO `views`(`id`, `count`) VALUES (1,1)";
	$result = mysqli_query($connection, $query);
    if(!$result) echo "Error Insert";
	return;
};

if($num > 0){
    $row=mysqli_fetch_array($result, MYSQLI_ASSOC);
    $i = $row['count'] + 1;
    echo "<p align='center'>Данная веб-страница была посещена: $i раз</p>";
    $query = "UPDATE `views` SET `count`='$i' WHERE `views`.`id`='1'";
    $result = mysqli_query($connection, $query);
    if(!$result) echo "Error Update";
}

?>