<?php 
    $host="localhost";
    $user="root";
    $password="";

    $connection=mysqli_connect($host, $user, $password) or mysqli_connect_error();
    if(!$connection) echo $connection;

    $query = "SHOW DATABASES;";
    $result = mysqli_query($connection, $query);

    $DBcreated = false;
    while($row=mysqli_fetch_array($result, MYSQLI_ASSOC)){
        if ($row['Database'] == 'guestbook') {
            $DBcreated = true;
            $db_status = 'Database Created and Working Fine';
        }
    }
    if (!$DBcreated) {
        $db_status = 'Database Initializing...';
        include_once "DatabaseInit.php";
    }
?>