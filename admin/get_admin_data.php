
<?php
    require "../connectToDB.php"; 

    session_start();

    $query = "SELECT * FROM `users` WHERE role = 1";

    $result = mysqli_query($connection, $query);

    if($result){
        $row=mysqli_fetch_array($result, MYSQLI_ASSOC);
        if(!isset($_SESSION['login'])) {
            $_SESSION['login'] = $row['login'];
        }
        $_SESSION['email'] = $row['email'];
        $_SESSION['date'] = $row['date'];
    }
    else {
        echo mysqli_error($connection);
    }
?>