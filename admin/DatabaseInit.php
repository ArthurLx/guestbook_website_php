<?php
    mysqli_set_charset($connection, 'utf-8');

    echo 'START DATABASE CREATION';

    // DATABASE GUESTBOOK
    $query='CREATE DATABASE IF NOT EXISTS  `guestbook`';
    
    $result=mysqli_query($connection,$query);
    if (!$result) {
        echo mysqli_error($connection);
        return;
    }

    $query='USE `guestbook`';
    $result=mysqli_query($connection,$query);
    if (!$result) {
        echo mysqli_error($connection);
        return;
    }

    // TABLE USERS
    $query='CREATE TABLE IF NOT EXISTS `users`(`id` INT UNSIGNED NOT NULL AUTO_INCREMENT, 
                                            `login` TEXT NOT NULL, 
                                            `password` TEXT NOT NULL, 
                                            `email` TEXT NOT NULL, 
                                            `email_hash` TEXT NOT NULL, 
                                            `email_confirmed` BIT NOT NULL, 
                                            `time` TEXT NOT NULL, 
                                            `date` TEXT NOT NULL, 
                                            `role` BIT NOT NULL, PRIMARY KEY(`id`))';
    
    $result=mysqli_query($connection,$query);
    if (!$result) {
        echo mysqli_error($connection);
        return;
    }

    // ADMIN ACCOUNT CREATION
    $login = 'Admin';
    $pass = password_hash('RootAdmin01', PASSWORD_DEFAULT);
    $email = 'admin@guestbook.com';
    $email_hash = md5($email);
    date_default_timezone_set('Europe/Kiev');
    $time = date("H:i:s");
    $date = date("d/m/y");
    $query = "INSERT INTO `users` (`id`, `login`, `password`, `email`, `email_hash`, `email_confirmed`,`time`,`date`,`role`) 
                        VALUES (NULL, '$login', '$pass', '$email', '$email_hash', 1, '$time', '$date', 1)";
    
    $result=mysqli_query($connection, $query);
    if (!$result) {
        echo mysqli_error($connection);
        return;
    }
                                                        
    // TABLE MESSAGES
    $query='CREATE TABLE IF NOT EXISTS `messages`(`id` INT UNSIGNED NOT NULL AUTO_INCREMENT, 
                                                `time` TEXT NOT NULL, `date` TEXT NOT NULL, 
                                                `name` TEXT NOT NULL,`email` TEXT NOT NULL, 
                                                `message` TEXT NOT NULL, PRIMARY KEY(id))';
    
    $result=mysqli_query($connection, $query);
    if (!$result) {
        echo mysqli_error($connection);
        return;
    }

    // TABLE VIEWS
    $query='CREATE TABLE IF NOT EXISTS `views` (`id` INT UNSIGNED NOT NULL AUTO_INCREMENT, 
                                                `count` INT UNSIGNED, PRIMARY KEY(id))';
    
    $result=mysqli_query($connection,$query);
    if (!$result) {
        echo mysqli_error($connection);
        return;
    }

    $DBcreated = true;
?>