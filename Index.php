<!DOCTYPE html>
<html>
<head>
    <title>Guest Book</title>
    <link href="style.css" rel="stylesheet">
</head>
<body>
    <header>
        <div class="flex-container">
            <a href="Index.php"><div class="flex-elem">Главная</div></a>
            <a href="search/SearchForm.php"><div class="flex-elem">Поиск</div></a>
            <a href="account/Register.php"><div class="flex-elem">Регистрация</div></a>
            <a href="account/Login.php"><div class="flex-elem">Вход</div></a>
        </div>
    </header>

    <main>
        <br>
        <h1 align="center">Гостевая книга</h1>
        <br>
        <div align="center">
            <?php include_once "PrintDB_not_auth.php"; ?>
        </div>

        <div class="floating_block">
            <img src="images/phone.png" alt="phone_logo" width="70" height="70"/>
            Reception: 0-800-111-111 Manager: 050-111-1111 <br> Address: Center 7 Street, Kyiv, UA01001
        </div>
    </main>

    <br><br><br><br><br><br>
</body>
</html>